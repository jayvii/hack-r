#  Copyright (C) 2019 Jan "JayVii"
#  Author 2019 Jan "JayVii" <jayvii@posteo.de>
#  SPDX-License-Identifier: GPL-3.0
#  About this header: <https://reuse.software>

# statusbar for loops and other index-driven iterations -----------------------

statusbar <- function(i, I, c80 = TRUE, perc = TRUE, stepsize = 1, pch = "."){

    # set modulo according to whether percentages shall be shown or not
    if (perc) mod <- 75 else mod <- 80

    # calculate modulo for column-80-rule
    if (c80) m80 <- i %% (mod * stepsize) else m80 <- -1

    # if new line starts, print percentage
    if (m80 == 1){
        ## calculate current progress in percent
        ip <- round((i / I) * 100)
        ## fill up chars until status is 3 characters long
        while (nchar(ip) < 3) ip <- paste0(" ", ip)
        ## finally, print percentage
        cat(paste0(ip, "% "))
    }

    # for every step, print a dot
    if (i %% stepsize == 0) cat(pch)

    # if a column is full, do a line-break (column-80-rule)
    if (m80 == 0) cat("\n")

}

# EOF statusbar.R

